import {defineStore, StateTree} from "pinia";
import {
    FlowError,
    LoginFlow,
    RegistrationFlow,
    Session,
    UpdateLoginFlowBody,
    UpdateRegistrationFlowBody
} from "@ory/kratos-client";
import router from "@/router";
import ory from "@/ory/sdk";
import {handleFlowError} from "@/ory/errors";
import {AxiosError} from "axios";

export interface kratosStore extends StateTree {
    session: Session | null
    error: FlowError | null
    loginFlow: LoginFlow | null
    registrationFlow: RegistrationFlow | null
    logoutToken: string | null
}

export const useKratosStore = defineStore('kratos', {
    state: (): kratosStore => {
        return {
            session: null,
            error: null,
            loginFlow: null,
            registrationFlow: null,
            logoutToken: null,
        }
    },
    actions: {
        submitLogin(values: UpdateLoginFlowBody) {
            const flow = this.loginFlow!;
            return router.push(`/login?flow=${flow.id}`)
                .then(() => {
                    ory.kratos.updateLoginFlow({
                        flow: flow.id,
                        updateLoginFlowBody: values,
                    })
                        .then(() => {
                            if (flow.return_to) {
                                window.location.href = flow.return_to
                                return
                            }
                            router.push('/')
                        })
                        .then(() => {
                        })
                        .catch(handleFlowError(router, 'login', () => this.loginFlow = null))
                        .catch((err: AxiosError) => {
                            // If the previous handler did not catch the error it's most likely a form validation error
                            if (err.response?.status === 400) {
                                // Yup, it is!
                                this.loginFlow = err.response!.data as LoginFlow;
                                return
                            }
                        })
                })
        },

        submitRegistration(form: UpdateRegistrationFlowBody) {
            const flow = this.registrationFlow!;
            return router
                .push(`/registration?flow=${flow.id}`)
                .then(() => {
                    ory.kratos.updateRegistrationFlow({
                        flow: flow.id,
                        updateRegistrationFlowBody: form
                    })
                        .then(() => {
                            if (flow.return_to) {
                                window.location.href = flow.return_to
                                return
                            }
                            router.push('/')
                        })
                        .then(() => {
                        })
                        .catch(handleFlowError(router, 'registration', () => this.registrationFlow = null))
                        .catch((err: AxiosError) => {
                            // If the previous handler did not catch the error it's most likely a form validation error
                            if (err.response?.status === 400) {
                                // Yup, it is!
                                this.registrationFlow = err.response!.data as RegistrationFlow
                                return
                            }
                        })
                })
        },
    }
})
