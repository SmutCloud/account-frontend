import {UiNode, UiNodeInputAttributes, UiNodeTypeEnum} from "@ory/kratos-client";

export type ValueSetter = (
    value: string | number | boolean | undefined
) => void

export type FormDispatcher = (e: MouseEvent | FormDataEvent) => Promise<void>

export interface NodeInputProps {
    node: UiNode
    attributes: UiNodeInputAttributes
    value: any
    disabled: boolean
    dispatchSubmit: FormDispatcher
    setValue: ValueSetter
}

export function isUiNodeInputAttributes(node: UiNode) {
    return node.attributes != undefined && node.attributes.node_type === UiNodeTypeEnum.Input;
}