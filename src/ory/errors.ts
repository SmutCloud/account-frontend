import {Router} from "vue-router";
import {AxiosError} from "axios";


export function handleFlowError<S>(
    router: Router,
    flowType: 'login' | 'registration' | 'settings' | 'recovery' | 'verification',
    resetFlow: any,
) {
    return async (err: AxiosError) => {
        // @ts-ignore
        switch (err.response?.data.error?.id) {
            case 'session_aal2_required':
                // 2FA is enabled and enforced, but user did not perform 2fa yet!
                // @ts-ignore
                window.location.href = err.response?.data.redirect_browser_to
                return
            case 'session_already_available':
                // User is already signed in, let's redirect them home!
                await router.push('/')
                return
            case 'session_refresh_required':
                // We need to re-authenticate to perform this action
                // @ts-ignore
                window.location.href = err.response?.data.redirect_browser_to
                return
            case 'self_service_flow_return_to_forbidden':
                // The flow expired, let's request a new one.
                // toast.error('The return_to address is not allowed.')
                resetFlow(undefined)
                await router.push('/' + flowType)
                return
            case 'self_service_flow_expired':
                // The flow expired, let's request a new one.
                // toast.error('Your interaction expired, please fill out the form again.')
                resetFlow(undefined)
                await router.push('/' + flowType)
                return
            case 'security_csrf_violation':
                // A CSRF violation occurred. Best to just refresh the flow!
                // toast.error(
                //     'A security violation was detected, please fill out the form again.'
                // )
                resetFlow(undefined)
                await router.push('/' + flowType)
                return
            case 'security_identity_mismatch':
                // The requested item was intended for someone else. Let's request a new flow...
                resetFlow(undefined)
                await router.push('/' + flowType)
                return
            case 'browser_location_change_required':
                // Ory Kratos asked us to point the user to this URL.
                // @ts-ignore
                window.location.href = err.response.data.redirect_browser_to
                return
        }

        switch (err.response?.status) {
            case 410:
                // The flow expired, let's request a new one.
                resetFlow(undefined)
                await router.push('/' + flowType)
                router.go(0);
                return
            case 404:
                // The flow apparently never existed, let's request a new one.
                resetFlow(undefined)
                await router.push('/' + flowType)
                router.go(0);
                return
            case 400:
                // validation error, this should be rejected as more cleanup is required, but it needs resetting here to
                // ensure changes propagate
                resetFlow();
        }

        // We are not able to handle the error? Return it.
        return Promise.reject(err)
    }
}
