import {Configuration, FrontendApi} from "@ory/kratos-client";


const baseUrlInternal =
    // @ts-ignore
    import.meta.env.VITE_ORY_SDK_URL;

const apiBaseFrontendUrlInternal =
    // @ts-ignore
    import.meta.env.VITE_ORY_KRATOS_PUBLIC_URL || baseUrlInternal
const apiBaseOauth2UrlInternal =
    // @ts-ignore
    import.meta.env.VITE_ORY_HYDRA_PUBLIC_URL || baseUrlInternal

export const apiBaseUrl =
    // @ts-ignore
    import.meta.env.VITE_ORY_KRATOS_BROWSER_URL || apiBaseFrontendUrlInternal

const hydraBaseOptions: any = {}

// if (process.env.MOCK_TLS_TERMINATION) {
//     hydraBaseOptions.headers = { "X-Forwarded-Proto": "https" }
// }

// Sets up the SDK
const sdk = {
    basePath: apiBaseFrontendUrlInternal,
    kratos: new FrontendApi(
        new Configuration({
            basePath: apiBaseUrl,
        }),
    ),
}

export default sdk
