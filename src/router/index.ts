import {createRouter, createWebHistory} from 'vue-router'
import IndexView from "@/views/IndexView.vue"
import LoginView from "@/views/LoginView.vue"
import ErrorView from "@/views/ErrorView.vue"
import RegistrationView from "@/views/RegistationView.vue"

const routes = [
  {
    path: '/',
    name: 'index',
    component: IndexView,
  },
  {
    path: '/error',
    name: 'error',
    component: ErrorView,
  },
  {
    path: '/login',
    name: 'login',
    component: LoginView
  },
  {
    path: '/registration',
    name: 'registration',
    component: RegistrationView
  },
]

const router = createRouter({
    history: createWebHistory(
        // @ts-ignore
        import.meta.env.BASE_URL
    ),
    routes
})
router.beforeEach(async (to) => {
  if (to.name) {
    document.body.dataset.route = to.name.toString();
  } else {
    delete document.body.dataset.route;
  }
})

export default router
